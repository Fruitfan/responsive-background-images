/**
 * Used for responsive background images
 */

class ResponsiveImageManager {
	constructor () {
		this.parsePage();
	}
	
	/**
	 * Finds all elements on the page with a srcset and adds responsive capabilities to them.
	 */
	parsePage () {
		let elements = document.querySelectorAll('[data-srcset]');

		let index = elements.length;
		while(index--) {
			let element = elements[index];
			this.addImage(element);
		}
	}

	/**
	 * Add an image element to the elements to use the native browser image functions
	 * @param {HTMLElement} element 
	 */
	addImage (element) {
		// 
		let image = document.createElement('img');
		image.srcset = element.getAttribute('data-srcset');
		image.src = element.getAttribute('data-src');
		image.sizes = element.getAttribute('data-sizes');

		image.style.display = 'none';
		image.style.position = 'absolute';
		image.style.top = '0';
		image.style.left = '0';

		element.appendChild(image);

		this.addImageEventListeners(image, element);
	}

	/**
	 * Add event listeners to the image element
	 * @param {HTMLMediaElement} image 
	 */
	addImageEventListeners (image, element) {
		image.addEventListener('load', () => {
			let imagesrc = image.currentSrc;

			// internet explorer does not support currentSrc, so use src instead.
			if (typeof imagesrc === 'undefined') imagesrc = image.src;

			element.style.backgroundImage = 'url(' + imagesrc + ')';
		});
	}
}

export default ResponsiveImageManager;