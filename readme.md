# Responsive Background Images

Adds responsive capabilities to elements with background images on the page.  

## Installation

    npm install --save https://Fruitfan@bitbucket.org/Fruitfan/responsive-background-images.git

## Usage
Mark up your HTML elements:  

```html
    <div
        data-src="http://example.com/image.png"
        data-srcset="http://example.com/image_200 200w,
                     http://example.com/image_100 100w"
        data-sizes="(min-width: 200px) 200w, 100w"
    ></div>
```

Then, load the library in JS:

```javascript
    import ResponsiveBgImages from 'responsive-background-images';
    const bgImageManager = new ResponsiveBgImages();
```

As soon as the manager is loaded, it will parse the page and look for elements with the attribute `data-srcset`. On browsers that support responsive image loading, it will use the image urls provided in `data-srcset` to choose the fitting image for the current size. Also use `data-sizes` to define the right size for the current screen size. Use `data-src` as a fallback.